const router = require('express').Router()
const teacherController = require('../controller/teacherController')
const alunoController = require('../controller/alunoController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')


router.get('/teacher/', teacherController.getTeachers);
router.post('/cadastroAluno', alunoController.postAlunos);
router.put('/cadastroAluno', alunoController.updateAlunos);
router.delete('/deleteAluno/:id', alunoController.deleteAlunos);
router.get("/external/", JSONPlaceholderController.getUsers);
router.get("/external/IO", JSONPlaceholderController.getUsersWebsiteIO);
router.get("/external/COM", JSONPlaceholderController.getUsersWebsiteCOM);
router.get("/external/NET", JSONPlaceholderController.getUsersWebsiteNET);

router.get("/external/filter", JSONPlaceholderController.getCountDomain);



module.exports = router