const axios = require("axios");

module.exports = class JSONPlaceholderController {
  static async getUsers(req, res) {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data;
      res
        .status(200)
        .json({
          message:
            "Aqui estão os usuários captados da Api pública JSONPlaceholder",
          users,
        });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Falha ao encontrar usuários" });
    }
  }

  static async getUsersWebsiteIO(req,res){
    try{
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      const users = response.data.filter(
        (user)=> user.website.endsWith(".io")
      )
      const Banana = users.length
      res.status(200).json({
          message:
            "AQUI ESTÃO OS USERS COM DOMINIO IO. Quantos bananas tem esse dominio:",Banana,
          users,
        });
    }
    catch(error){
      console.log(error)
      res.status(500).json({error:"Deu ruim",error})
    }
  }

  static async getUsersWebsiteCOM(req,res){
    try{
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/users"
        );
        const users = response.data.filter(
          (user)=> user.website.endsWith(".COM")
        )
        const Banana = users.length
        res.status(200).json({
            message:
              "AQUI ESTÃO OS USERS COM DOMINIO COM. Quantos bananas tem esse dominio:",Banana,
            users,
          });
      }
      catch(error){
        console.log(error)
        res.status(500).json({error:"Deu ruim",error})
      }
  }
  static async getUsersWebsiteNET(req,res){
    try{
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/users"
        );
        const users = response.data.filter(
          (user)=> user.website.endsWith(".NET")
        )
        const Banana = users.length
        res.status(200).json({
            message:
              "AQUI ESTÃO OS USERS COM DOMINIO NET. Quantos bananas tem esse dominio:",Banana,
            users,
          });
      }
      catch(error){
        console.log(error)
        res.status(500).json({error:"Deu ruim",error})
      }
  }
  
  static async getCountDomain(req,res){
    const { dominio } = req.query;

    const response = await axios.get(
      "https://jsonplaceholder.typicode.com/users"
    );

    if (dominio != "") {
    const users = response.data.filter(
      (user)=> user.website.endsWith(dominio)
    )
    const qtd = users.length
    res.status(200).json({
        message:
          `Quantidade de usuários com o domínio ${dominio}: ${qtd}.`
      });
  }else{
    res.status(400).json({error:"Insira um dominio"})
  }
}
}
